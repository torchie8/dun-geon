# Dun-geon  
### A text-based roguelike RPG/adventure

**Author:** Matyas Cerny (Matt Black)  
**Initial release date:** 12/19/2016

Dun-geon is a spinoff of my earlier game [DunMash](https://bitbucket.org/torchie8/dunmash). The aim of this project is mostly educational, to further my knowledge of C++, the OOP paradigm and various useful programming practices.  
My main goal while working on this project was and always will be to create everything from scratch. I want to avoid using any third-party libraries such as ncurses while still keeping the game portable between Linux and Windows.  
The project is still in a very early stage, with the first working test build with functional display and no major bugs released on 12/24/2016.

For help related to the game itself, an ingame help section will be implemented once the game is in an actually playable state.

### Licensing

Dun-geon is released free of charge as free software under the terms of the GNU General Public License v3. This means that anyone may freely access, run, study and modify the program and its source code and distribute verbatim copies of it as well as copies that have been modified (provided it is also under the terms of the GNU GPLv3) among other things.  
For more information, see LICENSE.txt.

### Build instructions

- Clone the repository.
- To build for Linux, run `make`.
- To build for Windows, run `make win`.
